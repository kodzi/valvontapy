Mikäli käyttöoikeudet puuttuu palautetaan 401 Unauthorized

GET api/settings/camera
Kuvaus: hakee kameran tilaraportin
Käyttöoikeus: registeröity
Vastaus: 200
{
    'flash_mode': 'smart',  //smart, always, disabled
    'flash_state': false,
    'camera_angle': 0,             // read-only
    'allow_increase': true, // read-only
    'allow_decrease': true  // read-only
}

POST api/settings/camera
Kuvaus: Muokkaa kameran kuvanotto-ominaisuudet
Käyttöoikeus: superuser ja staff
Pyyntö: data
{
    'flash_mode': 'always',  //Smart, Always, Disabled
    'capture_dimension': '2592x1944',
    'thumb_dimension': '259x144',
}
Vastaus: 200
{
    'flash_mode': 'always',  //Smart, Always, Disabled
    'capture_dimension': '2592x1944',
    'thumb_dimension': '259x144',
}
GET api/settings/camera/flash
Kuvaus: hakee flash-valon tila
Käyttöoikeus: registeröity
Vastaus: 200
{
    flash_state: false,
}
Virhe: 500
{
    error_message: ’Failed to retrieve flash-state’
}

POST api/settings/camera/flash
Kuvaus: asettaa flash-valon päälle (ON) tai pois (OFF)
Käyttöoikeus: superuser tai staff
Pyyntö: data
{
    flash_state: true,          // valinnainen
}
Vastaus: 200
{
    flash_state: true,
}
Virheet: 500
{
    message: ’Failed to set flash or change mode’
}

GET api/settings/camera/angle
Kuvaus: hakee kameran osoittama kulmaa
Käyttöoikeus: registeröity
Palautetyyppi: application/json
Vastaus: 200
{
    camera_angle: -90,
    allow_right: true,
    allow_left: false,
}

POST api/settings/camera/angle
Kuvaus: siirtää katselukulma joko nostamalla (oikealle) tai laskemalla (vasemmalle)
Käyttöoikeus: superuser tai staff
Pyyntö:
{
    direction: 'right'       // tai left
}
Vastaus: 200
{
    angle: -84,375,
    allow_right: true,
    allow_left: true
}
Virhe: 403
{
    message: 'Failed'
}

GET api/database/images
Kuvaus: hakee tiedot kuvista
Käyttöoikeus: registeröity
Pyyntö: data
{
    start_time: YYYY-MM-DD hh:mm
    end_time: YYYY-MM-DD hh:mm
    mode: all
}
Vastaus: 200
{
    results: [
    {
        date: create_at,
        full_image_path: MEDIA_URL/full_image_path,
        thumb_image_path: MEDIA_URL/full_image_path,
        capture_angle: camera_angle,
        thumb_width: thumb_width, thumb_height: thumb_height,
        capture_width: capture_width, capture_height: capture_height,
        flash: flash
    },
    ...
    ]
}
Virhe: 403

GET api/tools/auto_location
Kuvaus: hakee järjestelmän paikannuksen ja siihen liittyvät tiedot
Käyttöoikeus: registeröity
Palautetyyppi: application/json
Vastaus: 200
{
    "today_sunset": "2015-10-15 18:13:49",
    "city": "Tampere",
    "today_sunrise": "2015-10-15 08:07:46",
    "region": "Finland",
    "longitude": 23.75,
    "latitude": 61.5,
    "timezone": "Europe/Helsinki"
}

GET api/tools/system_report
Kuvaus: hakee järjestelmän tilaraportin
Käyttöoikeus: superuser tai staff
Palautetyyppi: application/json
Vastaus: 200
{
    camera:{
        'flash_mode': 'smart',  // Smart, Always, Disabled
        'flash_state': false,
        'angle': 0,
        'allow_right': true,
        'allow_left': true,
        'capture_dimension': '2592x1944',
        'thumb_dimension': '480x360',
    },
    sensors: {
        'motion_sensor': true,
        'sun_tracker': true
    },
    system: {
        local_ip: '127.0.0.1',
        public_ip: '89.32.32.1',
        uptime: 'xxYxxMMxWxDxHxMxS',
        boottime: '2015-10-11 12:20:05'
        storage: {
            disk_free: 4.10,        // in GB
            disk_total: 8.00,       // in GB
            image_database: 2.10,   // in GB
        }
    },
        
    location: {
        city: 'Tampere',
        region: 'Finland',
        timezone: 'Europe/Helsinki'
        latitude: 13.0,
        longitude: 62.0,
        today_sunrise: '2015-10-11 07:58:00',
        today_sunset: '2015-10-11 16:02:00',        
    }
}