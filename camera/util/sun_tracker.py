__author__ = "Hieu Nguyen"

from datetime import datetime, timedelta
from astral import Astral, Location, AstralError

from camera.models import Setting

class SunTracker(object):
    """
        This class is used to calculate sunrise and sunset time of a pre-defined
        location in the Setting-table or takes a dict() parameter with following
        keys: (city, latitude, longitude, region, timezone). Use update-method
        to update the sunrise and sunset time in the Setting.
    """
    def __init__(self, location):
        if location is not None:
            for key in location.keys():
                setattr(self, key, location[key])
        try:
            self.location = Astral()[self.city]
        except KeyError:
            loc = (
                self.city, self.region, self.latitude,
                self.longitude, self.timezone, 0
            )
            self.location = Location(loc)

    def get_sunrise_and_sunset(self, date=None):
        """
            Get Sunrise and sunset time with given date or calculate
            using current system time.
        """
        date = datetime.today() if date is None else date

        data = {
            'sunrise': self.get_sunrise(),
            'sunset': self.get_sunset()
        }
        return data

    def get_sunrise(self, date=None):
        """
            Takes datetime.datetime object as parameter or use current 
            system time as when without.
            returns datetime.datetime object
        """
        try:
            return self.location.sunrise(date, True)
        except AstralError:
            return "The sun doesn't rise on this date"

    # Takes datetime.datetim objsect as parameter
    # returns datetime.datetim object
    def get_sunset(self, date=None):
        """
            Takes datetime.datetime object as a parameter or use current 
            system time as when without.
            returns datetime.datetime object
        """
        date = datetime.today() if date is None else date
        try:
            return self.location.sunset(date, True)
        except AstralError:
            return "The sun doesn't set on this date"

    def is_dark(self):
        today = datetime.today()
        rise = self.get_sunrise()
        sset = self.get_sunset()
        before_rise = False
        after_set = False
        is_dark = False
        if isinstance(rise, datetime) and today < rise:
            before_rise = True
        if isinstance(sset, datetime) and today > sset:
            after_set = True
        if before_rise and after_set:
            is_dark = True
        return is_dark