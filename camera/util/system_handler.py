__author__ = "Hieu Nguyen"

import os
import requests
from PIL import Image as pil
from datetime import timedelta, datetime
from django.conf import settings
from tracking.models import Visitor
from camera.models import Setting, Image
from camera.util.sun_tracker import SunTracker
from camera.util.camera_handler import camera_handler
from camera.models import CAPTURE_DIMENSIONS, LIVE_DIMENSIONS, FLASH_MODES

MAX_CAMERA_ANGLE = 90
MIN_CAMERA_ANGLE = -90
DEFAULT_LOCATION = {
    'city': Setting.location_settings.city,
    'latitude': Setting.location_settings.latitude,
    'longitude': Setting.location_settings.longitude,
    'region': Setting.location_settings.region,
    'timezone': Setting.location_settings.timezone
}
SUN_TRACKER = SunTracker(DEFAULT_LOCATION)
LIVE_PERIOD = 5          # Seconds after last activity
THUMB_WIDTH = 120
THUMB_HEIGHT = 90
LIVE_IMAGE = None
def get_network():
    """
        Returns local and public ip of this system. Public-IP is retrieved
        using a 3rd-party API. In case of an API being down another API will be 
        used until a result is acquired.
        format returned: dict
    """
    local_ip = get_local_ip()
    public_ip = get_public_ip()
    data = {'public_ip': public_ip, 'local_ip': local_ip}
    return data

def get_local_ip():
    """
        Utilizes socket to try connect to Google server to retrieve the
        interface used to connect with outside world, the IP-address of this
        interface will be returned
        format returned: string
    """
    import socket

    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Try google server
    soc.connect(('8.8.8.8', 80))
    local_ip = soc.getsockname()[0]
    soc.close()
    return local_ip

def get_public_ip():
    """
        Try different APIs to retrieve public IP-address of this system
        format returned: string
    """
    ip_apis = (
        # service, url, response_tag
        ('42', 'http://ip.42.pl/raw', None),
        ('JsonIP', 'http://jsonip.com', 'ip'),
        ('httpbin', 'http://httpbin.org/ip', 'origin'),
        ('telize', 'http://www.telize.com/jsonip', 'ip'),
        ('ipify', 'https://api.ipify.org/?format=json', 'ip'),
    )
    for api in ip_apis:
        try:
            response = requests.get(api[1])
            if api[2] is None:
                public_ip = response.content
            else:
                public_ip = response.json()[api[2]]
            break
        except:
            # ignore exception and try another api
            print "Failed to retrieve ip from %s, trying next api" % (api[0])
    return public_ip

def get_geo():
    """
        Try different APIs to retrieve a set of values related to system's
        geolocation.
        format returned: dict
        example: {city: Tampere, region: Finland, lat: 61.5, lon: 23.75, 
            timezone: Europe/Helsinki}
    """
    geolocation_apis = (
        (
            'Ip-Api', 'http://ip-api.com/json/'+get_public_ip(),
            ('country', 'city', 'lat', 'lon', 'timezone'), #78ms
        ),
        (
            'ShinyGeoip', 'http://geoip.nekudo.com/api/'+get_public_ip()+'/simple',
            ('country', 'city', 'location', 'location', 'location'), #142ms
        ),
        (
            'freegeoip', 'https://freegeoip.net/json/'+get_public_ip(),
            ('country_name', 'city', 'latitude', 'longitude', 'time_zone'), #493ms
        ),
    )

    api_format = ('region', 'city', 'latitude', 'longitude', 'timezone')

    content = dict()
    for api in geolocation_apis:
        try:
            response = requests.get(api[1])
            for idx, entry in enumerate(api_format):
                data = response.json()[api[2][idx]]
                if isinstance(data, dict):
                    if entry == 'region':
                        data = data.get('name')
                    elif entry == 'lat':
                        data = data.get('latitude')
                    elif entry == 'lon':
                        data = data.get('longitude')
                    elif entry == 'timezone':
                        data = data.get('time_zone')
                if entry == 'lat' or entry == 'lon':
                    data = float(data)
                content[entry] = data
            break
        except:
            # ignore exception and try another api
            print "Failed to retrieve geolocation from %s, trying next api" % (api[0])
    return content

def get_system():
    """
        Retrieves and return a set of state system values, including storage, 
        uptime, boottime, and network.
        format returned: dict
    """
    from uptime import uptime, boottime

    uptime_str = str(timedelta(seconds=uptime()))
    boot_date = boottime().strftime("%Y-%m-%d %H:%M:%S")
    data = {
        'uptime': uptime_str, 'boottime': boot_date,
        'storage': storage(),
        'local_ip': get_local_ip(), 'public_ip': get_public_ip()
    }
    return data

def get_camera():
    """
        Returns current state of the camera. DB values: camera_flash_mode,
        camera_angle, allow_left, allow_right, capture_dimension and live_dimension
        format returned: dict
    """
    keys = (
        'camera_flash_mode', 'camera_flash', 'camera_angle',
        'live_dimension', 'capture_dimension'
    )
    data = dict()
    for key in keys:
        data[key] = getattr(Setting.capture_settings, key)

    allow_left = False if data['camera_angle'] <= MIN_CAMERA_ANGLE else True
    allow_right = False if data['camera_angle'] >= MAX_CAMERA_ANGLE else True
    data['allow_left'] = allow_left
    data['allow_right'] = allow_right
    return data

def set_camera(query):
    """
        Set camera settings, supported keys are: camera_flash_mode,
        capture_dimension and live_dimension, returns nothing.
    """
    try:
        supported_keys = (
            'capture_dimension', 
            'live_dimension', 
            'camera_flash_mode'
        )
        for entry in supported_keys:
            query_val = query[entry]
            if query_val is not None:
                new_value = None
                choices = None
                if entry == supported_keys[0]:
                    choices = CAPTURE_DIMENSIONS
                elif entry == supported_keys[1]:
                    choices = LIVE_DIMENSIONS
                elif entry == supported_keys[2]:
                    choices = FLASH_MODES
                for val in choices:
                    if query_val in val:
                        new_value = query_val
                        break
                if new_value is not None:
                    setattr(Setting.capture_settings, entry, val[0])

    except Exception as ex:
        raise ex

def set_camera_angle(direction):
    """
        Set camera angle to "left" or "right" and modify camera_angle variable in
        Setting.capture_settings, returns nothing.
    """
    try:
        camera_angle = Setting.capture_settings.camera_angle
        if direction == 'right' and camera_angle < MAX_CAMERA_ANGLE:
            change = camera_handler.set_angle(direction)
            Setting.capture_settings.camera_angle += change
            camera_angle = Setting.capture_settings.camera_angle
        elif direction == 'left' and camera_angle > MIN_CAMERA_ANGLE:
            change = camera_handler.set_angle(direction)
            Setting.capture_settings.camera_angle += change
            camera_angle = Setting.capture_settings.camera_angle
    except Exception as ex:
        raise ex

def set_camera_flash(state):
    """
        Set camera flash to True or False and update camera_flash variable in
        Setting.capture_settings, returns nothing
    """
    try:
        camera_flash = camera_handler.set_flash(state)
        Setting.capture_settings.camera_flash = True if camera_flash else False
    except Exception as ex:
        raise ex

def get_sensors():
    """
        Retrieves and returns a list of sensors in the system
        format returned: {(sensor_name, state),(sensor_name, state)}
    """
    sensors_list = Setting.sensors.keys()
    data = dict()
    for sensor in sensors_list:
        data[sensor] = getattr(Setting.sensors, sensor)
    return data

def set_sensors(query):
    """
        Activate or deactivate a set of sensor, check get_sensors() to
        see a list of those available.
    """
    try:
        sensors_list = Setting.sensors.keys()
        for sensor in sensors_list:
            if query[sensor] is not None:
                setattr(Setting.sensors, sensor, query[sensor])
    except Exception as ex:
        raise ex

def get_location():
    """
        Retrieves and returns a list of variables related to location of the system
        format returned: dict()
    """
    keys = (
        'city', 'region', 'timezone', 'latitude',
        'longitude', 'today_sunrise', 'today_sunset'
    )
    data = dict()
    for key in keys:
        data[key] = getattr(Setting.location_settings, key)
    return data

def set_location(query):
    """
        Set system geolocation and related settings. Supported keys:
        city (str), region (str), timezone(str), latitude(float), longitude(float),
        today_sunrise(str), today_sunset(str)
    """
    supported_keys = (
        'city', 'region', 'timezone', 'latitude', 'longitude'
    )
    try:
        new_loc = dict()
        for key in supported_keys:
            setattr(Setting.location_settings, key, query[key])
            new_loc[key] = query[key]

        # Update SUN_TRACKER with new settings
        SUN_TRACKER = SunTracker(new_loc)
    except Exception as ex:
        raise ex

def get_auto_location():
    """
        Using 3rd-part API to retrieve and return location specific values such as
        geolocation and sunrise/sunset.
        format returned: dict
    """
    location_geo = get_geo()
    return location_geo

def storage():
    """
        Calculate disk space and the amount of images captured excluding 
        thumbnails.
        format returned: dict
    """
    #from django.conf import settings
    import os
    from camera.models import Image

    stat = os.statvfs('/')
    #st_media = os.statvfs(settings.MEDIA_ROOT)
    if stat.f_frsize:
        disk_free = _convert_bytes(stat.f_frsize * stat.f_bavail)
        disk_total = _convert_bytes(stat.f_frsize * stat.f_blocks)
        #image_database = _convert_bytes(
            #st_media.f_frsize * (st_media.f_blocks - st_media.f_bavail))
    else:
        disk_free = _convert_bytes(stat.f_bsize * stat.f_bavail)
        disk_total = _convert_bytes(stat.f_bsize * stat.f_blocks)
        #image_database = _convert_bytes(
            #st_media.f_frsize * (st_media.f_blocks - st_media.f_bavail))
    images_count = Image.objects.count()
    data = {
        'disk_free': disk_free, 'disk_total': disk_total,
        #'image_database': image_database,
        'images_count': images_count
    }
    return data

def update_image():
    """
    Capture a new image if requirements met: motion detected or live viewers exists.
    """
    flash_mode = Setting.capture_settings.camera_flash
    # Backup current flash state
    current_flash = camera_handler.get_flash()
    flash_state = current_flash
    live_viewers = Visitor.objects.all().filter(url='/live/image/')
    # Activate flash according to settings
    if not current_flash:
        if flash_mode == 'always':
            camera_handler.set_flash(True)
            flash_state = True
        elif flash_mode == 'smart' and Setting.sensors.sun_tracker:
            if SUN_TRACKER.is_dark():
                camera_handler.set_flash(True)
                flash_state = True

    c_dim = Setting.capture_settings.capture_dimension
    for typ in CAPTURE_DIMENSIONS:
        if c_dim == typ[1]:
            c_dim = typ[0]
            break
    f_width = int(c_dim.split('x')[0])
    f_height = int(c_dim.split('x')[1])
    # Case 1: Motion detected
    if camera_handler.motion_detected():
        # Update stream with maximum capture size
        camera_handler.update_image(f_width, f_height)
        _save_capture(f_width, f_height, flash_state)
    # Case 2: Live view update
    elif live_viewers:
        # An active user has requested a live image atleast LIVE_PERIOD amount of seconds ago.
        if (timedelta(seconds=LIVE_PERIOD) + live_viewers[0].last_update) > datetime.utcnow():
            l_dim_type = Setting.capture_settings.live_dimension
            for typ in LIVE_DIMENSIONS:
                if l_dim_type == typ[1]:
                    l_dim = typ[0]
                    break
            l_width = int(l_dim.split('x')[0])
            l_height = int(l_dim.split('x')[1])
            camera_handler.update_image(l_width, l_height)
            _update_live_image()
    # Return flash to it last state
    camera_handler.set_flash(current_flash)

def _save_capture(f_width, f_height, flash_state):
    """
    Save capture to database with fullsize and its thumbnail
    """
    img = pil.open(camera_handler.get_image())
    now = datetime.today().strptime('%Y/%m/%d/%H_%M_%S')
    f_name = now + '.jpg'
    t_name = now + '_thumb' + '.jpg'
    try:
        img_db = Image.objects.create(
            full_image_path=settings.MEDIA_URL+f_name,
            thumb_image_path=settings.MEDIA_URL+t_name,
            flash=flash_state,
            capture_angle=Setting.capture_settings.camera_angle,
            capture_width=f_width,
            capture_height=f_height,
            thumb_width=THUMB_WIDTH,
            thumb_height=THUMB_HEIGHT
        )
        # Save full size
        img_copy = img.copy()
        img_copy.save(os.path.join(settings.MEDIA_ROOT, f_name))
        # Save thumb
        img_copy.thumbnail((THUMB_WIDTH, THUMB_HEIGHT))
        img_copy.save(os.path.join(settings.MEDIA_ROOT, t_name))
        # Save entry to DB
        img_db.save()
        return True
    except:
        return False

def _update_live_image():
    """
    Update current live view image with correct width and height
    """
    global LIVE_IMAGE
    LIVE_IMAGE = camera_handler.get_image().copy()

def get_live_image():
    """
    Return current Live image for view
    """
    return LIVE_IMAGE

# Return amount of free space in bytes
def _convert_bytes(amount):
    """
        Convert a float-number to a string with a suffix closest to it's size.
        format returned: string
    """
    for tag in ['bytes', 'KB', 'MB', 'GB']:
        if amount < 1024.0:
            return "%.1f%s" % (amount, tag)
        amount /= 1024.0
    return "%.1f%s" % (amount, 'TB')
