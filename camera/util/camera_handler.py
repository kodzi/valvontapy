try:
    import picamera
    import RPi.GPIO as GPIO
except:
    pass

import io
import time
from PIL import Image

ANGLE_PER_CMD = 5.625       # degress
STEPS_PER_CMD = 64          # equals 5.625
SEQ_REST_TIME = 0.001       # SEQ speed lower = faster
LEDS_PIN = 23               # GPIO pin to control LED(s)
PIR_PIN = 24
MOTOR_PINS = [22, 27, 17, 4]   # Step-motor
MOTOR_CTRL_SEQ = (
    (1, 0, 0, 0),
    (1, 1, 0, 0),
    (0, 1, 0, 0),
    (0, 1, 1, 0),
    (0, 0, 1, 0),
    (0, 0, 1, 1),
    (0, 0, 0, 1),
    (1, 0, 0, 1)
)
MOTOR_CTRL_SEQ_LEN = len(MOTOR_CTRL_SEQ)
ERROR_ROTATION_ONGOING = "Rotation on going, please try again later"

class CameraHandler(object):
    """
        A camera class for controlling hardware camera and its 
        peripherals, avoid creating multiple objects by importing 
        using a module instead:
        from camera_handler import camera_handler
        This way there will be no conflict on the hardware side.
    """
    rotation_ongoing = False
    capture_ongoing = False
    stream = None
    def __init__(self):
        try:
            self.GPIO = GPIO
            self.GPIO.setmode(GPIO.BCM)
            #Set pins controlling stepper motor as output
            for pin in MOTOR_PINS:
                self.GPIO.setup(pin, GPIO.OUT)
                self.GPIO.output(pin, False)

            #Set led light pin as output        
            self.GPIO.setup(LEDS_PIN, GPIO.OUT)

            #Set PIR pin as output
            self.GPIO.setup(PIR_PIN, GPIO.OUT)
        except:
            print "NOTE: camera_handler Failed to initialize GPIO pins as OUTPUT"

    def __del__(self):
            self.GPIO.cleanup()

    def set_flash(self, state):
        """
            Control hardware state of flash
        """
        try:
            if state != self.get_flash():
                self.GPIO.output(LEDS_PIN, state)
            return self.get_flash()
        except Exception as ex:
            raise ex

    def get_flash(self):
        """
            Get Hardware state of flash
        """
        try:
            return self.GPIO.input(LEDS_PIN)
        except Exception as ex:
            raise ex

    def set_angle(self, direction):
        """
            Accepts a string input being either left/right and rotate the hardware 
            camera accordingly ANGLE_PER_CMD degrees
            returns: change of angle been made
        """
        if self.rotation_ongoing:
            raise Exception(ERROR_ROTATION_ONGOING)
        if direction == "right":
            change = self._rotate_right()
        else:
            change = -self._rotate_left()
        return change

    def _rotate_right(self, angle=None):
        """
            Control hardware step motor rotation to RIGHT by float angle-variable or without.
            return angle rotated
        """
        self.rotation_ongoing = True

        if angle is not None:
            total_steps = int(((STEPS_PER_CMD*angle)/ANGLE_PER_CMD) + 0.5)
        else:
            total_steps = STEPS_PER_CMD

        seq_count = 7
        step_count = 0

        # Start counter-clockwise loop
        #print "NOTE: camera_handler rotation disabled"
        while step_count <= total_steps:
            for pin in range(0, 4):
                xpin = MOTOR_PINS[pin]
                if MOTOR_CTRL_SEQ[seq_count][pin] != 0:
                    self.GPIO.output(xpin, True)
                    
                else:
                    self.GPIO.output(xpin, False)
                    
            seq_count -= 1
            step_count += 1

            # If we reach the end of the sequence
            # start again
            if seq_count < 0:
                seq_count = 7

            # Wait before moving on
            time.sleep(SEQ_REST_TIME)
        self.rotation_ongoing = False
        rotated_angle = ANGLE_PER_CMD if angle is None else angle
        return rotated_angle

    def _rotate_left(self, angle=None):
        """
            Control hardware step motor rotation to LEFT by float angle-variable or without.
            return angle rotated
        """
        # Activate rotation
        self.rotation_ongoing = True

        if angle is not None:
            total_steps = int(((STEPS_PER_CMD*angle)/ANGLE_PER_CMD) + 0.5)
        else:
            total_steps = STEPS_PER_CMD

        seq_count = 0
        step_count = 0

        #print "NOTE: camera_handler rotation disabled"
        # Start clockwise loop
        while step_count <= total_steps:
            for pin in range(0, 4):
                xpin = MOTOR_PINS[pin]
                if MOTOR_CTRL_SEQ[seq_count][pin] != 0:
                    self.GPIO.output(xpin, True)
                    
                else:
                    self.GPIO.output(xpin, False)
            seq_count += 1
            step_count += 1

            # If we reach the end of the sequence
            # start again
            if seq_count == MOTOR_CTRL_SEQ_LEN:
                seq_count = 0
            if seq_count < 0:
                seq_count = MOTOR_CTRL_SEQ_LEN

            # Wait before moving on
            time.sleep(SEQ_REST_TIME)

        self.rotation_ongoing = False
        rotated_angle = ANGLE_PER_CMD if angle is None else angle
        return rotated_angle

    def get_image(self):
        """
            Return fullsize image as BytesIO
        """
        self.stream.seek(0)
        return Image.open(self.stream)

    def update_image(self, width, height):
        """
            Update self.stream-variable with new fullsized capture as jpeg
        """
        if self.capture_ongoing:
            return
        self.capture_ongoing = True
        new_stream = io.BytesIO()
        with picamera.PiCamera() as camera:
            camera.start_preview()
            time.sleep(2)
            camera.capture(new_stream, format='jpeg', resize=(width, height))
        self.capture_ongoing = False
        self.stream = new_stream

    def motion_detected(self):
        """
        Returns True or False
        """
        return self.GPIO.input(PIR_PIN)
# Camera module
camera_handler = CameraHandler()
