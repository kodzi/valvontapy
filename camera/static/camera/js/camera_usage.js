var box_checked = 'fa-check-circle-o'
var box_unchecked = 'fa-circle-o'
var interval = null
var cam_api = '/api/settings/camera/'
// Flash mode
function flashHandler(){
    btn = $('#flash_check')
    current_ico = ""; next_ico = ""; new_state = "";
    if(btn.find('i').hasClass(box_unchecked)){
        new_state = true
        current_ico = box_unchecked
        next_ico = box_checked
    }
    else if(btn.find('i').hasClass(box_checked)){
        new_state = false
        next_ico = box_unchecked
        current_ico = box_checked
    }
    else if(btn.find('i').hasClass('fa-spinner')){
        return;
    }
    $.ajax({
        url : cam_api+'flash/', // the endpoint
        type : 'POST', // http method
        data : {'camera_flash': new_state},
        headers: { "X-CSRFToken": getCookie("csrftoken") },
        beforeSend: function(){
            btn.find('i').removeClass(current_ico).addClass('fa-spinner fa-spin');
        },
        // handle a successful response
        success : function(data) {
            btn.removeClass('disabled')
            if (data.camera_flash){
                btn.find('i').removeClass('fa-spinner fa-spin').removeClass(box_unchecked).addClass(box_checked);
            }
            else{
                btn.find('i').removeClass('fa-spinner fa-spin').addClass(box_unchecked).removeClass(box_checked);
            }
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            btn.find('i').removeClass('fa-spinner fa-spin').addClass(current_ico);
            showAlert('Command failed', err, 'warning')
        }
    });
}

// Auto-refresh
function autoRefreshHandler(){
    btn = $('#auto_refresh').find('i')
    if(btn.hasClass(box_unchecked)){
        // TODO enable auto-refresh
        interval = setInterval(refreshImage, 2000)
        btn.removeClass(box_unchecked).addClass(box_checked);
    }
    else if(btn.hasClass(box_checked)){
        // TODO disable auto-refresh
        clearInterval(interval)
        btn.removeClass(box_checked).addClass(box_unchecked);
    }
}

//Rotation
function rotateHandler(direction){
    btn = $('#rotate_'+direction)
    direction_2 = (direction == 'right') ? 'left':'right'
    btn_2 = $('#rotate_'+direction_2)
    ico = 'fa-chevron-'+direction
    $.ajax({
        url : cam_api+'angle/', // the endpoint
        type : 'POST', // http method
        data : {'direction': direction},
        headers: { "X-CSRFToken": getCookie("csrftoken") },
        beforeSend: function(){
            btn.addClass('disabled')
            btn_2.addClass('disabled')
            btn.find('i').removeClass(ico).addClass('fa-spinner fa-spin');
        },
        // handle a successful response
        success : function(data) {
            btn.find('i').removeClass('fa-spinner fa-spin').addClass(ico);
            btn_state = (direction == 'left') ? data.allow_left:data.allow_right
            btn_state2 = (direction == 'left') ? data.allow_right:data.allow_left
            buttonEnableToggle(btn, "no_change", "no_change", btn_state, !btn_state)
            buttonEnableToggle(btn_2, "no_change", "no_change" , btn_state2, !btn_state2 )
            $('#camera_angle').val(~~data.camera_angle)

        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            btn.find('i').removeClass('fa-spinner fa-spin').addClass(ico);
            btn.removeClass('disabled')
            btn_2.removeClass('disabled')
            showAlert('Command failed', err, 'warning')
        }
    });
}

function refreshImage(){
    console.log("update")
    //$('#live_image').attr("src", "/media/static.jpg?" + new Date().getTime());
    $('#live_image').attr("src", "/live/image?"+ new Date().getTime());
}

window.addEventListener("keydown", shortcutHandler, false);
 
function shortcutHandler(e) {
    switch(e.keyCode) {
        case 37:
            disabled = $('#rotate_left').hasClass('disabled')
            if (!disabled){
                $('#rotate_left').click()
            }
            break;
        case 39:
            disabled = $('#rotate_right').hasClass('disabled')
            if (!disabled){
                $('#rotate_right').click()
            }
            break;
    }   
}

function bindControls(evt){
    $('#flash_check').bind('click', function(evt){
        evt.preventDefault();
        flashHandler();
    });
    $('#rotate_left').on('click', function(evt){
        evt.preventDefault();
        rotateHandler('left')
    });
    $('#rotate_right').on('click', function(evt){
        evt.preventDefault();
        rotateHandler('right')
    });
    $('#manual_capture').on('click', function(evt){
        evt.preventDefault();
        manualCaptureHandler()
    });
    $('#auto_refresh').on('click', function(evt){
        evt.preventDefault();
        autoRefreshHandler()
    });
}

function initButtons(){
    $.ajax({
        url : cam_api, // the endpoint
        type : 'GET', // http method
        dataType: 'json',
        beforeSend: function(){
            $('#rotate_right').addClass('disabled')
            $('#rotate_left').addClass('disabled')
            $('#flash_check').addClass('disabled')
            $('#auto_refresh').addClass('disabled')
        },
        // handle a successful response
        success : function(data) {
            $('#auto_refresh').removeClass('disabled')
            buttonEnableToggle($('#flash_check'), box_unchecked, box_checked, data.camera_flash, false)
            buttonEnableToggle($('#rotate_right'), 'fa-chevron-right', 'fa-chevron-right', data.allow_right, !data.allow_right)
            buttonEnableToggle($('#rotate_left'), 'fa-chevron-left', 'fa-chevron-left', data.allow_left, !data.allow_left)

        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            showAlert('Initialization failed', errmsg, 'error')
        }
    });
}

function buttonEnableToggle(
    btn, old_ico, new_ico, 
    state, disable){

    // disable button or not according to disable-parameter
    if(!disable){
        btn.removeClass('disabled')
    }
    if (old_ico !== new_ico){
        // if change icon according to state
        if(state){
            btn.find('i').removeClass(old_ico).addClass(new_ico)
        }
        else{
            btn.find('i').removeClass(new_ico).addClass(old_ico)
        }
    }
}

jQuery(function(evt){
    initButtons()
    bindControls(evt);
})