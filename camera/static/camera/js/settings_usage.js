
var spinner_ico = "<i class='fa fa-spinner fa-spin'></i>"
var settings_api = '/api/settings/'
var tools_api = '/api/tools/'

function settingFormAjaxHandler(query, btn, url, callback, succ_template, err_template){
    btn_html = btn.html
    $.ajax({
        url : url, // the endpoint
        type : 'POST', // http method
        data : query,
        headers: { "X-CSRFToken": getCookie("csrftoken") },
        beforeSend: function(){
            btn.html = spinner_ico
            btn.addClass('disabled')
        },
        // handle a successful response
        success : function(data) {
            callback(data)
            btn.html = btn_html
            btn.removeClass('disabled')
            showAlert(succ_template,'', 'success')
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            btn.html = btn_html
            btn.removeClass('disabled')
            showAlert(err_template, err, 'warning')
        }
    });
}

function locationAutoComplete(){
    btn = $('#auto_complete')
    btn_html = btn.html
    $.ajax({
        url : tools_api+'auto_location/', // the endpoint
        type : 'GET', // http method
        beforeSend: function(){
            btn.html = spinner_ico
            btn.addClass('disabled')
        },
        // handle a successful response
        success : function(data) {
            set_location_form(data)
            btn.html = btn_html
            btn.removeClass('disabled')
            showAlert('Auto-retrieval succeeded','Please re-check and save save afterwards', 'success')
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            btn.html = btn_html
            btn.removeClass('disabled')
            showAlert('Auto-retrieval failed', err, 'warning')
        }
    });
}

var set_sensor_form = function setSensorForm(data){
    sensor_form = $('#sensor_form')
    // dynamically 
    for (var key in data) {
        key_val = (data[key]) ? 'on':'off'
        $('#'+key).bootstrapToggle(key_val)
    }
}
var set_location_form = function setLocationForm(data){
    $('#city').val(data.city)
    $('#region').val(data.region)
    $('#timezone').val(data.timezone)
    $('#latitude').val(data.latitude)
    $('#longitude').val(data.longitude)
    $('#today_sunrise').val(data.today_sunrise)
    $('#today_sunset').val(data.today_sunset)
}

var set_capture_form = function setCaptureForm(data){
    for (var key in data) {
        $('#'+key).val(data[key])
    }
}
function bindSaveButtons(evt){
    $('#capture_form').on('submit', function(evt){
        evt.preventDefault()
        console.log( $( this ).serialize() );
        succ = 'Capture settings updated'
        err = 'Updating capture settings failed'
        url = settings_api+'camera/'
        data = $( this ).serialize()
        btn = $('#capture_save')
        settingFormAjaxHandler(data, btn, url, set_capture_form, succ, err)
    })
    $('#location_form').on('submit', function(evt){
        evt.preventDefault()
        succ = 'Location settings changed'
        err = 'Changing location settings failed'
        url = settings_api+'location/'
        data = $( this ).serialize()
        btn = $('#location_save')
        settingFormAjaxHandler(data, btn, url, set_location_form, succ, err)
    })
    $('#sensor_form').on('submit', function(evt){
        evt.preventDefault()
        //sensorPOST($( this ).serialize());
        succ = 'Sensor settings updated'
        err = 'Updating sensor settings failed'
        url = settings_api+'sensors/'
        data = $( this ).serialize()
        btn = $('#sensor_save')
        settingFormAjaxHandler(data, btn, url, set_sensor_form, succ, err)
    })
    $('#location_auto').on('click', function(evt){
        evt.preventDefault()
        locationAutoComplete()
    })
    $("#camera_angle_slider").roundSlider({
        radius: 80,
        circleShape: "half-top",
        handleSize: "+16",
        handleShape: "dot",
        sliderType: "min-range",
        value: 65
    });
}
jQuery(function(evt){
    bindSaveButtons(evt);
})