function galleryFilterHandler(query, callback){
  btn = $('#gallery_filter_btn')
  btn_html = btn.html
  if( validateEmpty($('#start_time')) || validateEmpty($('#end_time'))){
    return
  }
  $.ajax({
    url: '/api/database/images',
    method:'GET',
    data: query,
    beforeSend: function(){
      $('#results').addClass('hidden')
      btn.html = "<i class='fa fa-spinner fa-spin'></i>"
      btn.addClass('disabled')
    },
    success: function(data){
      $('#gallery_results').html('')
      callback(data.results)
      btn.html = btn_html
      btn.removeClass('disabled')
    },
    error: function(xhr, errmsg, err){
      btn.html = btn_html
      btn.removeClass('disabled')
      showAlert('Failed retrieving images', err, 'warning')
    }
  })
}

var displayed_images = 0
var total_images = 0
var image_results
var process_results = function processResults(results){
  $('#results').html('<h3 class="text-center">Found '+results.length+' results</h3>')
  $('#results').removeClass('hidden')
  if(results.length === 0){
    showAlert('Results empty', 'No images found between this range', 'warning')
  }
  else
  {
    showAlert('Found images', 'A total of '+results.length+' between this range', 'success')
    displayed_images = 0
    total_images = results.length
    image_results = results
    addImages()
  }
}

// Add ten images at a time to results
function addImages(){
  i = 0
  for (; i < 10 && displayed_images+i < total_images; i++){
    console.log(image_results[displayed_images+i].created_at)
    var imgtag = document.createElement('img')
    imgtag.setAttribute('src', image_results[displayed_images+i].thumb_image_path)
    imgtag.setAttribute('width', 160)
    imgtag.setAttribute('height', 120)

    var divtag = document.createElement('div')
    divtag.className = 'grid-item'
    divtag.appendChild(imgtag)
    var destag = document.createElement('div')
    destag.className = 'grid-item-des'
    var text = document.createTextNode(image_results[displayed_images+i].created_at)
    destag.appendChild(text)

    divtag.appendChild(destag)
    var atag = document.createElement('a')
    atag.setAttribute('href',image_results[displayed_images+i].full_image_path);
    atag.appendChild(divtag)

    $('.grid-container').append(atag)
  }
  displayed_images += i
}

$(document).ready(function() {
  var options = {minMargin: 5, maxMargin: 15, itemSelector: ".grid-item", firstItemClass: "first-item"};
  $(".grid-container").rowGrid(options);
  var today = new Date();
  var yesterday = new Date();
  yesterday.setDate(today.getDate() - 1)
  $('#start_time').datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
    defaultDate: yesterday,
  });

  $('#end_time').datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
    defaultDate: today,
  });

  $('#gallery_filter').on('submit', function(evt){
    evt.preventDefault()
    galleryFilterHandler($(this).serialize(), process_results)

  })

  $('.grid-container').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
        addImages()
        $(".grid-container").rowGrid("appended");
      }
  })
});