// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

// Check if input is empty, mark parent err
function validateEmpty(inp){
    level = 'warning'
    inp.closest('div.has-feedback').removeClass('has-'+level)
    if(inp.val() === ""){
        inp.closest('div.has-feedback').addClass('has-'+level)
        showAlert('Missing data', inp.prop('placeholder')+' is empty', level)
        return true
    }
    else{
        inp.closest('div.has-feedback').addClass('has-success')
        return false
    }
}

function loginHandler(){
    username = $('input[name="username"]')
    password = $('input[name="password"]')
    login_ico = $('#auth_form').find('i')
    if( validateEmpty(username) || validateEmpty(password)){
        return
    }
    $.ajax({
        url : "/login", // the endpoint
        type : "POST", // http method
        data : $('#auth_form').serialize(),
        beforeSend: function(){
            login_ico.removeClass('fa-sign-in').addClass('fa-spinner fa-spin');
        },
        // handle a successful response
        success : function() {
            window.location.reload();
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            login_ico.removeClass('fa-spinner fa-spin').addClass('fa-sign-in');
            showAlert("Logging in failed!", xhr.responseText, 'danger');
        }
    });
}
function logoutHandler(){
    console.log("logout")
    login_ico = $('#auth_form').find('i')
    $.ajax({
        url : "/logout", // the endpoint
        type : "POST", // http method
        data : $('#auth_form').serialize(),
        beforeSend: function(){
            login_ico.removeClass('fa-sign-out').addClass('fa-spinner fa-spin');
        },
        // handle a successful response
        success : function() {
            window.location.reload();
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            login_ico.removeClass('fa-spinner fa-spin').addClass('fa-sign-out');

       }
    });
}

// level could be: info, danger, success or warning
function showAlert(title, message, level){
    $('#alert_title').html("")
    $('#alert_message').html("")
    $('#alert_ctn').removeClass('in')
    $('#alert_title').html(title)
    $('#alert_message').html(message)
    $('#alert_ctn').toggleClass('in').toggleClass('alert-'+level)
    setTimeout(function() {
        $('#alert_ctn').removeClass('in');
        $('#alert_ctn').removeClass('alert-'+level);
        $('#alert_title').html("")
        $('#alert_message').html("")
    }, 2000);
}

jQuery(function(){
    function bindButtons(){
        $('#auth_form').on('submit', function(evt){
            evt.preventDefault()
            if($('#auth_form').find('i').hasClass('fa-sign-in')){
                loginHandler()
            }
            else if($('#auth_form').find('i').hasClass('fa-sign-out')){
                logoutHandler()
            }
        });
    }
    bindButtons();
});