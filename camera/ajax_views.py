__author__ = "Hieu Nguyen"

import os
from datetime import datetime

from django.conf import settings
from django.http import JsonResponse, HttpResponse, Http404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.admin.views.decorators import staff_member_required

from PIL import Image as pil
from camera.models import Image
from camera.util import system_handler

METHOD_UNSUPPORTED = "Request method is not supported"
INCORRECT_INPUT = "The given input is incorrect"
TEST_TIME = 0

@staff_member_required
def settings_view(request, property_name=None, second_property=None):
    status = 403
    data = {'message': METHOD_UNSUPPORTED}
    if property_name == 'sensors':
        if second_property is None:
            status, data = _settings_sensors_handler(request)
    elif property_name == 'location':
        if second_property is None:
            status, data = _settings_location_handler(request)
    elif property_name == 'camera':
        if second_property is None:
            status, data = _camera_view_handler(request)
        elif second_property == 'angle':
            status, data = _camera_angle_handler(request)
        elif second_property == 'flash':
            status, data = _camera_flash_handler(request)
    return JsonResponse(data, status=status)

@login_required
def database_view(request):
    if request.method == 'GET':
        status = 200
        range_start = datetime.strptime(request.GET.get('start_time'), "%Y-%m-%d %H:%M")
        range_end = datetime.strptime(request.GET.get('end_time'), "%Y-%m-%d %H:%M")

        images = Image.objects.filter(created_at__range=[range_start, range_end]).order_by('-created_at')

        response_content = (
            'created_at', 'full_image_path', 'thumb_image_path',
            'flash', 'capture_angle', 'capture_width',
            'capture_height', 'thumb_width', 'thumb_height'
        )
        results = list()
        if images:
            for image in images:
                arg = dict()
                for key in response_content:
                    if key == 'created_at':
                        arg[key] = getattr(image, key).strftime('%y/%m/%d %H:%M:%S')
                    else:
                        arg[key] = getattr(image, key)
                results.append(arg)
        data = {'results': results}
    else:
        status = 403
        data= {'message': METHOD_UNSUPPORTED}
    return JsonResponse(data, status=status)

@login_required
def tools_view(request, property_name=None):
    if request.method == 'GET':
        status = 200
        if property_name == 'auto_location':
            data = system_handler.get_auto_location()
        if property_name == 'system_report':
            data = {
                'camera': system_handler.get_camera(),
                'sensors': system_handler.get_sensors(),
                'system': system_handler.get_system(),
                'location': system_handler.get_location()
            }
        return JsonResponse(data, status=status)

def _camera_view_handler(request):
    status = 200
    if request.method == 'GET':
        data = system_handler.get_camera()
    elif request.method == 'POST':
        query = {
            'camera_flash_mode': request.POST.get('camera_flash_mode'),
            'capture_dimension': request.POST.get('camera_dimension'),
            'live_dimension': request.POST.get('live_dimension')
        }
        try:
            system_handler.set_camera(query)
            data = system_handler.get_camera()
        except Exception as ex:
            status = 500
            data = {'message': ex}

    else:
        status = 403
        data = {'message': METHOD_UNSUPPORTED}
    return status, data

def _camera_angle_handler(request):
    status = 200
    if request.method == 'GET':
        response_content = ('camera_angle', 'allow_left', 'allow_right')
        cam_data = system_handler.get_camera()
        data = dict()
        for key in response_content:
            data[key] = cam_data.get(key)
    elif request.method == 'POST':
        direction = request.POST.get('direction')
        if  direction == 'right' or direction == 'left':
            try:
                system_handler.set_camera_angle(direction)
                response_content = ('camera_angle', 'allow_left', 'allow_right')
                cam_data = system_handler.get_camera()
                data = dict()
                for key in response_content:
                    data[key] = cam_data.get(key)
            except Exception as ex:
                status = 500
                data = {'message': ex}
        else:
            status = 403
            data = {'message': INCORRECT_INPUT}
    else:
        status = 403
        data = {'message': METHOD_UNSUPPORTED}
    return status, data

def _camera_flash_handler(request):
    status = 200
    if request.method == 'GET':
        response_content = ('camera_flash')
        cam_data = system_handler.get_camera()
        data = dict()
        for key in response_content:
            data[key] = cam_data.get(key)
    elif request.method == 'POST':
        req_state = request.POST.get('camera_flash')
        state = True if req_state == "true" or req_state == "True" else False
        system_handler.set_camera_flash(state)
        cam_data = system_handler.get_camera()
        data = {'camera_flash': cam_data.get('camera_flash')}
    else:
        status = 403
        data = {'message': METHOD_UNSUPPORTED}
    return status, data

def _settings_sensors_handler(request):
    status = 200
    if request.method == 'GET':
        data = system_handler.get_sensors()
    elif request.method == 'POST':
        req_sun = request.POST.get('sun_tracker')
        req_motion = request.POST.get('motion_sensor')
        accepted_values = ('on', 'ON', 'true', 'True')
        sun_state = True if req_sun in accepted_values else False
        motion_state = True if req_motion in accepted_values else False

        req_states = {
            'sun_tracker': sun_state,
            'motion_sensor': motion_state
        }
        system_handler.set_sensors(req_states)
        data = system_handler.get_sensors()
    else:
        status = 403
        data = {'message': METHOD_UNSUPPORTED}
    return status, data

def _settings_location_handler(request):
    status = 200
    if request.method == 'GET':
        data = system_handler.get_location()
    elif request.method == 'POST':
        query = dict()
        for key in request.POST.keys():
            query[key] = request.POST.get(key)
        system_handler.set_location(query)
        data = system_handler.get_location()
    else:
        status = 403
        data = {'message': METHOD_UNSUPPORTED}
    return status, data

@login_required
def live_image(request):
    global TEST_TIME
    if request.method == 'GET':
        if TEST_TIME == 8:
            TEST_TIME = 0
        response = HttpResponse(content_type="image/jpeg")
        if system_handler.get_live_image() is None:
            img = pil.open(settings.MEDIA_ROOT + 'static'+str(TEST_TIME)+'.jpg')
        else:
            img = system_handler.get_live_image()
        img.save(response, 'JPEG')
        TEST_TIME += 1
        return response
    else:
        raise Http404
