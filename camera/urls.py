from django.conf.urls import url, include
from django.views.generic.base import RedirectView

from . import views, ajax_views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^live/$', views.index, name='live'),
    url(r'^live/image/$', ajax_views.live_image, name='live_image'),
    url(r'^gallery/$', views.gallery, name='gallery'),
    url(r'^settings/$', views.settings, name='settings'),

    url(r'^login', views.login),
    url(r'^logout', views.logout),

    url(r'^api/database/images', ajax_views.database_view),
    url(r'^api/settings/(?P<property_name>\w+)/$', ajax_views.settings_view),
    url(r'^api/settings/(?P<property_name>\w+)/(?P<second_property>\w+)/$', ajax_views.settings_view),

    url(r'^api/tools/(?P<property_name>\w+)/$', ajax_views.tools_view),

    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/icos/favicon.ico', permanent=True)),
]