from django.contrib import admin

# Register your models here.
from camera.models import Image, Log

class ImageAdmin(admin.ModelAdmin):
    pass
class LogsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Image, ImageAdmin)
admin.site.register(Log, LogsAdmin)