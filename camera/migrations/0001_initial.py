# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(help_text=b'Datetime upon entry creation', auto_now_add=True)),
                ('file_path', models.CharField(help_text=b'filepath after MEDIA_ROOT-path in settings.py', max_length=256)),
                ('capture_mode', models.CharField(default=b'auto', max_length=6, choices=[(b'auto', b'Motion detected'), (b'manual', b'Manual')])),
                ('flash', models.BooleanField(default=False)),
                ('capture_angle', models.DecimalField(help_text=b'amount of digits allowed is 5 from which 2 is decimal', null=True, max_digits=5, decimal_places=2, blank=True)),
                ('full_w', models.PositiveSmallIntegerField(blank=True)),
                ('full_h', models.PositiveSmallIntegerField(blank=True)),
                ('thumb_w', models.PositiveSmallIntegerField(blank=True)),
                ('thumb_h', models.PositiveSmallIntegerField(blank=True)),
                ('capturer', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
            ],
        ),
    ]
