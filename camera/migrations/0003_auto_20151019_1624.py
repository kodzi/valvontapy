# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('camera', '0002_add_service_settings'),
    ]

    operations = [
        migrations.RenameField(
            model_name='image',
            old_name='file_path',
            new_name='full_image_path',
        ),
        migrations.RemoveField(
            model_name='image',
            name='capture_mode',
        ),
        migrations.RemoveField(
            model_name='image',
            name='capturer',
        ),
        migrations.AddField(
            model_name='image',
            name='thumb_image_path',
            field=models.CharField(default=datetime.datetime(2015, 10, 19, 16, 24, 59, 968412), help_text=b'filepath after MEDIA_ROOT-path in settings.py', max_length=256),
            preserve_default=False,
        ),
    ]
