# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('camera', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RenameField(
            model_name='image',
            old_name='full_h',
            new_name='capture_height',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='full_w',
            new_name='capture_width',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='thumb_h',
            new_name='thumb_height',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='thumb_w',
            new_name='thumb_width',
        ),
    ]
