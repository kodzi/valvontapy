from django.core.management.base import BaseCommand, CommandErrory
from camera.util import system_handler

class Command(BaseCommand):
    help = 'Run periodic tasks'
    args = 'Arguments are not needed'
    
    def handle(self, *args, **options):
        system_handler.update_image()
