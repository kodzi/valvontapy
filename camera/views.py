from django.shortcuts import render
from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse

from camera.models import Setting
from camera.models import LIVE_DIMENSIONS, CAPTURE_DIMENSIONS, FLASH_MODES

from pytz import common_timezones

def index(request):
    return render(request, 'camera/index.html', {'activate_live':'active', 'camera_angle': Setting.capture_settings.camera_angle})

def settings(request):
    data = {
        'activate_settings': 'active',
        'sensors': Setting.sensors,
        'live_dimensions': LIVE_DIMENSIONS,
        'capture_dimensions': CAPTURE_DIMENSIONS,
        'flash_modes': FLASH_MODES,
        'location_settings': Setting.location_settings,
        'camera_angle': Setting.capture_settings.camera_angle,
        'timezones': common_timezones
    }
    return render(request, 'camera/settings.html', data)

def gallery(request):
    return render(request, 'camera/gallery.html', {'activate_gallery': 'active'})

def login(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect("/")
    else:
        return HttpResponse(status=401, content="Username and password mismatch")

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return HttpResponse(status=200)