from django.db import models
from django.contrib.auth.models import User

import dbsettings
from django.utils import timezone as tz

# Create your models here.
class Image(models.Model):
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True, help_text="Datetime upon entry creation")
    full_image_path = models.CharField(max_length=256, help_text="filepath after MEDIA_ROOT-path in settings.py")
    thumb_image_path = models.CharField(max_length=256, help_text="filepath after MEDIA_ROOT-path in settings.py")
    flash = models.BooleanField(default=False)
    capture_angle = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, help_text="amount of digits allowed is 5 from which 2 is decimal")
    capture_width = models.PositiveSmallIntegerField(blank=True)
    capture_height = models.PositiveSmallIntegerField(blank=True)
    thumb_width = models.PositiveSmallIntegerField(blank=True)
    thumb_height = models.PositiveSmallIntegerField(blank=True)

class Log(models.Model):
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    content = models.TextField()

MAX = 'Maximum'
HIGH = 'High'
MEDIUM = 'Medium'
SMALL = 'Small'
CAPTURE_DIMENSIONS = [
    ('2592x1944', MAX),
    ('1440x1080', HIGH),
    ('960x720', MEDIUM),
    ('720x540', SMALL)
]
LIVE_DIMENSIONS = (
    ('960x720', MAX),
    ('720x540', HIGH),
    ('640x480', MEDIUM),
    ('480x320', SMALL)
)
SMART = 'smart'
ALWAYS = 'always'
DISABLED = 'disabled'
FLASH_MODES = [
    (SMART, 'According to sunrise and sunset'),
    (ALWAYS, 'Always on during capture'),
    (DISABLED, 'Disabled')
]

class CaptureSettings(dbsettings.Group):
    capture_dimension = dbsettings.StringValue(default=MAX, choices=CAPTURE_DIMENSIONS, help_text='Quality of captured images for storage purposes, use smaller sizes to save disk space.')
    live_dimension = dbsettings.StringValue(default=MEDIUM, choices=LIVE_DIMENSIONS, help_text='Quality of thumb images created from captures for Web GUI, this will affect page loading time.')
    camera_flash_mode = dbsettings.StringValue(default=SMART, choices=FLASH_MODES, help_text='Enable flash to help capturing images in the dark.')
    camera_flash = dbsettings.BooleanValue(default=False, help_text="Current state of flash, WARNING: this value is readonly as it is modified automatically")
    camera_angle = dbsettings.FloatValue(default=0, help_text="Please input a value between -90.000 and 90.000 only, WARNING: this field should not be edited without inspection of current physical angle.")


class LocationSettings(dbsettings.Group):
    city = dbsettings.StringValue('Name of city', default="Tampere", help_text='check http://pythonhosted.org/astral/#cities for supported city names')
    region = dbsettings.StringValue('Name of region', default="Finland", help_text='Optional field')
    latitude = dbsettings.FloatValue(default=23.7667, help_text="Latitude is needed when the city isn't in the supported city list.")
    longitude = dbsettings.FloatValue(default=61.5000, help_text="Longititude is needed when the city isn't in the supported list.")
    timezone = dbsettings.StringValue(default="Europe/Helsinki", help_text="Timezone is needed when the city isn't in the supported city list.")
    #today_sunrise = dbsettings.StringValue(default='2015-10-11 17:42:00', help_text="This field will be auto-updated if Sun tracker is enabled, the format is YYYY-MM-DD hh:mm:ss")
    #today_sunset = dbsettings.StringValue(default='2015-10-11 17:43:00', help_text="This field will be auto-updated if Sun tracker is enabled, the format is YYYY-MM-DD hh:mm:ss")

class Sensors(dbsettings.Group):
    motion_sensor = dbsettings.BooleanValue('Motion sensor', default=True, help_text="Detects motion, uncheck to disable")
    sun_tracker = dbsettings.BooleanValue('Sun tracker', default=True, help_text="Retrieves state of the Sun for sunrise and sunset time, uncheck to disable")

class Setting(models.Model):
    capture_settings = CaptureSettings('Capture settings')
    sensors = Sensors('Sensors')
    location_settings = LocationSettings('Geographic location')
