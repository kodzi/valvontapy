A wireless camera surveillance system based on Raspberry Pi and Pi camera module

### About ###

* Web GUI
* Support for camera rotating
* Support PIR motion detection to auto capture image
* Flash-mode based on Sunset/sunrise

### How do I get set up? ###

* Clone
* virtualenv
* install requirements
* run

### Who do I talk to? ###

* email me